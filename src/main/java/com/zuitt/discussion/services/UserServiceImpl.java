package com.zuitt.discussion.services;

import com.zuitt.discussion.models.User;
import com.zuitt.discussion.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

public class UserServiceImpl implements UserService{

    @Autowired
    private UserRepository userRepository;

    public Optional<User> findByUsername(String username){
        // if findByUserName returns null, it will throw a NullPointerException
        // using .ofNullable() will avoid this from happening, it will return null instead
        return Optional.ofNullable(userRepository.findByUsername(username));
    }

}
