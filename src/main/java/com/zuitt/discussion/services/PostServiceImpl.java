//contains the business logic concerned with a particular object in the class
package com.zuitt.discussion.services;

import com.zuitt.discussion.models.Post;
import com.zuitt.discussion.repositories.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service //allow us to use the CRUD repository methods inherited from the CRUDRepository
public class PostServiceImpl implements PostService{
    // an object cannot be instantiated from interfaces
    // @Autowired allow us to use the interface as if it was an instance of an object
    // and allows us to use the methods from the CrudRepository
    @Autowired
    private PostRepository postRepository;

    public void createPost(Post post){
        postRepository.save(post);
    }

    public Iterable<Post> getPosts(){
        return postRepository.findAll();
    }

    public ResponseEntity deletePost(Long id) {
        postRepository.deleteById(id);
        return new ResponseEntity<>("Post deleted successfully.", HttpStatus.OK);
    }

    public ResponseEntity upatePost(Long id, Post post){
        Post postForUpdate = postRepository.findById(id).get();

        postForUpdate.setTitle(post.getTitle());
        postForUpdate.setContent(post.getContent());

        postRepository.save(postForUpdate);

        return new ResponseEntity<>("Post updated successfully", HttpStatus.OK);
    }
}
